from django.conf import settings
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from accounts.forms import CadastroUsuarioForm


def cadastrar(request):
    context = {}
    template_name = 'accounts/cadastro.html'
    form = CadastroUsuarioForm()

    if request.method == 'POST':
        form = CadastroUsuarioForm(request.POST)

        if form.is_valid():
            user = form.save()
            user = authenticate(username=user.username,
                                password=form.cleaned_data['password1']
                                )
            login(request, user)

            return redirect(settings.LOGIN_REDIRECT_URL)

    context['form'] = form

    return render(request, template_name, context)
