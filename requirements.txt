django==2.0.1
gunicorn==19.7.1
django-s3-storage
psycopg2==2.7.3.2
mysqlclient
Pillow
selenium
django-debug-toolbar
