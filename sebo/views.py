from django.core.paginator import Paginator
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.conf import settings
from sebo.models import Vendedor, Livro
from sebo.forms import BookSearch, LivroForm


def index(request):
    template_name = 'sebo/index.html'
    context = {}

    ultimos_sebos = Vendedor.objects.order_by('-cadastrado_em')[:10]
    qtd_sebos = Vendedor.objects.count()
    qtd_livros = Livro.objects.count()

    context['ultimos_sebos'] = ultimos_sebos
    context['qtd_sebos'] = qtd_sebos
    context['search_form'] = BookSearch()
    context['qtd_livros'] = qtd_livros
    context['title'] = 'Livros novos e usados'

    return render(request, template_name, context)


def search(request):
    template_name = 'sebo/book_search.html'
    context = {'title': 'Busca'}

    if request.method == 'GET':
        search_form = BookSearch(request.GET)

        if search_form.is_valid():
            resultado_busca = Livro.objects.prefetch_related('autores').filter(nome__icontains=search_form.cleaned_data['q'])

            paginador = Paginator(resultado_busca, 20)
            pagina = request.GET.get('page', 1)

            context['resultado_busca'] = paginador.get_page(pagina)
            context['qtd_livros_encontrados'] = paginador.count
            context = {'title': 'Busca por ' + search_form.cleaned_data['q']}

            return render(request, template_name, context)
        else:
            # TODO: colocar mensagem
            pass

    return redirect('')


def detalhes_livro(request, slug):
    template_name = 'sebo/book.html'
    context = {}

    livro = get_object_or_404(Livro, slug=slug)

    context['livro'] = livro

    return render(request, template_name, context)


def detalhes_vendedor(request, slug):
    context = {}
    template_name = 'sebo/vendedor.html'

    vendedor = get_object_or_404(Vendedor.objects.select_related('user'), slug=slug)

    eh_dono_visualizando = request.user.pk == vendedor.user.pk
    paginador = Paginator(Livro.objects.prefetch_related('autores').filter(vendedor__id=1), 10)
    pagina = request.GET.get('page')
    livros = paginador.get_page(pagina)

    context['vendedor'] = vendedor
    context['livros'] = livros
    context['eh_o_dono_visualizando'] = eh_dono_visualizando
    context['qtd_livros'] = paginador.count

    if eh_dono_visualizando:
        context['title'] = 'Meus livros'
    else:
        nome_vendedor = vendedor.nome
        if nome_vendedor:
            context['title'] = nome_vendedor

    return render(request, template_name, context)


@login_required
def add_livro(request, slug):
    context = {}
    template_name = 'sebo/add_livro.html'
    form = LivroForm()

    if request.method == 'POST':
        vendedor = get_object_or_404(Vendedor, user=request.user)

        if request.user != vendedor.user:
            return redirect(settings.LOGIN_URL)

        form = LivroForm(request.POST, request.FILES, instance=Livro(vendedor=vendedor))

        if form.is_valid():
            livro = form.save()

            return redirect(livro)

    context['form'] = form
    context['title'] = 'Vender um livro'

    return render(request, template_name, context)
