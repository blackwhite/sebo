"""sebomania URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from sebo.views import index, search, detalhes_livro, detalhes_vendedor, add_livro

app_name = 'sebo'


urlpatterns = [
    path('', index, name='index'),
    path('search/', search, name='search'),
    path('livros/<slug:slug>/', detalhes_livro, name="detalhes_livro"),
    path('vendedores/<slug:slug>/livros', detalhes_vendedor, name="detalhes_vendedor"),
    path('vendedores/<slug:slug>/livros/vender_livro', add_livro, name="vender_livro")
    ]
