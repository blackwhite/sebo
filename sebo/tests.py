from django.test import TestCase, SimpleTestCase, RequestFactory
from django.contrib.auth.models import User
from django.urls import reverse
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
import sebo.views as sebo_views
from sebo.forms import BookSearch, VendedorForm
from sebo.models import Vendedor, Autor
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait

import pdb
"""
class HomeFunctionalTest(StaticLiveServerTestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def test_query(self):
        self.selenium.get(self.live_server_url + '/')
        search_input = self.selenium.find_element_by_name('q')
        search_input.send_keys('olavo')

        self.selenium.find_element_by_id('bt-procurar').click()

        # se a página for gerada via javscript vai dar erro
        WebDriverWait(self.selenium, 2).until(
            lambda driver: driver.find_element_by_tag_name('body')
        )

        h1_container = self.selenium.find_element_by_xpath('//div[@class="content"]/h1')

        self.assertEquals(h1_container.text, 'Nenhum livro encontrado')
"""


class HomeViewTest(TestCase):

    def test_get_home(self):
        resp = self.client.get('/')

        self.assertEquals(resp.status_code, 200)

    def test_search_book_index(self):
        resp = self.client.get('/search', {'q': 'ooo'}, follow=True)

        self.assertEquals(resp.status_code, 200)
        self.assertEquals(resp.context['qtd_livros_encontrados'], 0)
        self.assertTrue('Nenhum livro encontrado' in str(resp.content))


class AutorModelTest(TestCase):

    def setUp(self):
        self.autor = Autor.objects.create(nome='Olavo de Carvalho',
                                          sobre='Ele tem razão',
                                          slug='olavo-de-carvalho'
                                          )

    def test_verbose_name_plural(self):
        self.assertEquals(self.autor._meta.verbose_name_plural, 'autores')

    def test_verbose_name(self):
        self.assertEquals(self.autor._meta.verbose_name, 'autor')

    def test_string_representation(self):
        self.assertEquals(str(self.autor), 'Olavo de Carvalho')


    def tearDown(self):
        self.autor.delete()

class BookSearchFormTest(TestCase):

    def test_invalid_empty_form_(self):
        form = BookSearch()

        self.assertFalse(form.is_valid())
        self.assertEquals(form.errors, {})

    def test_valid_form(self):
        form = BookSearch({'q': '1984'})

        self.assertTrue(form.is_valid())
        self.assertEquals(form.cleaned_data['q'], '1984')



class VendedorFormTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.usuario = User.objects.create_user('oswssaldo', 'vasssldo@gmail.com', 'tesssste_dddd')

    def test_invalid_empty_form(self):
        form = VendedorForm()

        self.assertFalse(form.is_valid())

    def test_invalid_form_invalid_data(self):
        data = {'sobre': 'Pobre'}

        form = VendedorForm(data)

        self.assertFalse(form.is_valid())
        self.assertEquals(form.errors,
            {
             'nome': ['Este campo é obrigatório.']
             }
        )

    def test_valid_form(self):
        usuario = User.objects.create_user('oswaldo', 'valdo@gmail.com', 'teste_dddd')

        data = {'sobre': 'Um qualquer',
                'nome': 'Jonas',
        }

        vv = Vendedor(user=self.usuario, slug='jonas')

        form = VendedorForm(data, instance=vv)
        ehValido = form.is_valid()

        self.assertTrue(form.is_valid())
