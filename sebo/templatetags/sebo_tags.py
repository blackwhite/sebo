import locale
from urllib.parse import urlencode
from django import template
from sebo.forms import BookSearch

register = template.Library()

locale.setlocale(locale.LC_ALL, 'pt_BR.utf8')

@register.simple_tag(takes_context=True)
def url_replace(context, **kwargs):
    query = context['request'].GET.dict()
    query.update(kwargs)

    return urlencode(query)


@register.filter
def formatar_moeda(value):
    return locale.currency(value)


@register.inclusion_tag('sebo/book_search_nav.html')
def show_book_search_nav():
    form = BookSearch()

    form.fields['q'].widget.attrs.update({'class': 'input'})

    return {'search_form': form}
