import itertools
from decimal import Decimal
from django.db import models
from django.urls import reverse
from django.utils.text import slugify
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.validators import MinValueValidator


class Vendedor(models.Model):

    nome = models.CharField('Nome do sebo', max_length=100)
    sobre = models.TextField('Sobre', max_length=200, blank=True, null=True)
    cadastrado_em = models.DateTimeField('Data de cadastro', auto_now_add=True)
    slug = models.SlugField('Slug')

    user = models.OneToOneField(User, on_delete=models.CASCADE)

    SLUG_FIELDS = ['nome']


    def get_absolute_url(self):
        return reverse('sebo:detalhes_sebo', kwargs={'slug': self.slug})


    class Meta:
        verbose_name = 'sebo'
        verbose_name_plural = 'sebos'
        indexes = [
            models.Index(fields=['slug'])
        ]


def generate_slug(model, fields=None):
    fields = fields or model.SLUG_FIELDS
    dados_para_slug = '-'.join([getattr(model, field) for field in fields ])

    slug = slugify(dados_para_slug)

    total = model.__class__.objects.filter(slug=slug).count()

    if total:
        for x in itertools.count(total):
            slug_pesquisar = slug + '-' + str(x)

            if not model.__class__.objects.filter(slug=slug_pesquisar).exists():
                model.slug = slug_pesquisar
                break

    else:
        model.slug = slug

@receiver(post_save, sender=User)
def criar_vendedor(sender, instance, created, **kwargs):
    if created:
        if not (instance.is_superuser or instance.is_staff):
            nome_completo = instance.first_name

            if instance.last_name:
                nome_completo = nome_completo + ' ' + instance.last_name

            v = Vendedor(nome=nome_completo, user=instance)
            generate_slug(v, ['nome'])

            v.save()


class Autor(models.Model):

    nome = models.CharField('Nome', max_length=60)
    sobre = models.TextField('Sobre', max_length=200, null=True)
    slug = models.SlugField('Slug')


    class Meta:
        verbose_name = 'autor'
        verbose_name_plural = 'autores'

    def __str__(self):
        return self.nome


class Livro(models.Model):

    nome = models.CharField('Nome', max_length=150)
    descricao = models.TextField('Descrição', max_length=1200, null=True)
    ano = models.IntegerField('Ano de lançamento')
    qtd_paginas = models.PositiveIntegerField('Quantidade de Páginas')
    cadastrado_em = models.DateTimeField('Cadastrado em', auto_now_add=True)
    atualizado_em = models.DateTimeField('Atualizado_em', auto_now=True)
    slug = models.SlugField('Slug')

    foto = models.ImageField(verbose_name='Foto do livro',
                             upload_to='imagens_livros', blank=True,
                             null=True)

    autores = models.ManyToManyField(Autor)
    vendedor = models.ForeignKey(Vendedor, on_delete=models.CASCADE)
    preco = models.DecimalField('Preço', max_digits=12, decimal_places=2,
                                validators=[MinValueValidator(Decimal('1.0'))])

    SLUG_FIELDS = ('nome', 'nome_autores')


    def get_absolute_url(self):
        return reverse('sebo:detalhes_livro', kwargs={'slug': self.slug})

    @property
    def nome_autores(self):
        return ','.join([autor.nome for autor in self.autores.all()])

    class Meta:
        verbose_name = 'livro'
        verbose_name_plural = 'livros'
        indexes = [
            models.Index(fields=['slug'])
        ]
