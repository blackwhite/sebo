import operator
from functools import reduce
from django import forms
from django.forms import ModelForm
from django.db.models import Q
from sebo.models import Livro, Autor, Vendedor, generate_slug

class BookSearch(forms.Form):

    q = forms.CharField(label='Pesquisa',
                             max_length=100,
                             required=True,
                             widget=forms.TextInput(attrs={'placeholder': 'Que livro você procura?',
                                                           'class': 'input is-medium'}))


class VendedorForm(ModelForm):

    class Meta:
        model = Vendedor
        fields = ['nome', 'sobre']

    def save(self, commit=True):
        generate_slug(self.instance)

        return super(VendedorForm, self).save()


class LivroForm(ModelForm):

    autores = forms.CharField(
        label='Autores',
        help_text='Use a vírgula para separar múltiplos autores',
        required=True
        )

    class Meta:
        model = Livro
        fields = ['nome', 'descricao', 'ano', 'qtd_paginas', 'foto', 'preco']

    def __init__(self, *args, **kwargs):
        super(LivroForm, self).__init__(*args, **kwargs)

        for field in self.fields.values():
            field.widget.attrs.update({'class': 'input'})

        self.fields['descricao'].widget.attrs.update({'class': 'textarea'})

    def save(self, commit=True):
        autores = []
        autores_form = self._get_autores()

        qs = [Q(nome__iexact=autor) for autor in autores_form]

        if len(qs) == 1:
            qs = qs[0]
        else:
            qs = reduce(operator.or_, qs)

        autores_filtrados = Autor.objects.filter(qs)

        if len(autores_filtrados) != len(autores_form): # TODO: problema com nomes iguais
            autores_nao_cadastrados = set([x.lower() for x in autores_form]) - set([autor.nome.lower() for autor in autores_filtrados])

            for nome in autores_nao_cadastrados:
                autor = Autor(nome=nome)
                autor.save()

                autores.append(autor)

        for autor in autores_filtrados:
            autores.append(autor)


        livro = super(LivroForm, self).save()
        self.instance.autores.set(autores)

        if commit:
            livro.save()
            generate_slug(self.instance)
            livro.save()

        return livro

    def _get_autores(self, sep=','):
        return self.cleaned_data['autores'].split(sep)
