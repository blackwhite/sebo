FROM ubuntu:latest
RUN apt-get update && apt-get -y upgrade

RUN apt-get -y install \
    python3-dev python3-pip

RUN apt-get install -y libmysqlclient-dev

RUN apt-get -y install locales
RUN locale-gen pt_BR.utf8

RUN mkdir app
RUN mkdir app/requirements

WORKDIR app

ADD requirements.txt requirements

RUN pip3 install -r requirements/requirements.txt

COPY sebomania ./sebomania
COPY sebo ./sebo
COPY accounts ./accounts
COPY static ./static

COPY gunicorn_config.py ./
COPY manage.py ./
COPY entrypoint_docker.sh ./

EXPOSE 8000

CMD gunicorn -c gunicorn_config.py -t 3600 sebomania.wsgi --reload
